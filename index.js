import Editor from './lib/editor.js';

import EventEmitter from './lib/EventEmitter.js';

import ColorManager from './lib/managers/Color.js';
import PaletteManager from './lib/managers/Palette.js';
import SettingsManager from './lib/managers/Settings.js';
import ToolManager from './lib/managers/Tool.js';

import ColorPicker from './lib/ui/ColorPicker.js';
import ExportWindow from './lib/ui/ExportWindow.js';
import Palette from './lib/ui/Palette.js';
import PaletteWindow from './lib/ui/PaletteWindow.js';
import SettingsWindow from './lib/ui/SettingsWindow.js';
import StartWindow from './lib/ui/StartWindow.js';
import ToolBox from './lib/ui/ToolBox.js';

import BucketTool from './lib/tools/bucket.js';
import DropperTool from './lib/tools/dropper.js';
import EraserTool from './lib/tools/eraser.js';
import LineTool from './lib/tools/line.js';
import PencilTool from './lib/tools/pencil.js';
import SelectTool from './lib/tools/select.js';


const eventEmitter = new EventEmitter();

// Window sizing
const mobileQuery = window.matchMedia('(pointer: coarse)');

const windowContainer = document.getElementById('modal-container');

// EDITOR
const editor = new Editor();

eventEmitter.on('editorConfigure', (args) => {
    editor.configure(args['width'], args['height']);

    eventEmitter.emit('closeStartWindow');
});

// START WINDOW
const startWindow = new StartWindow(
    eventEmitter, document.getElementById('start-modal'));

eventEmitter.on('closeStartWindow', () => {
    windowContainer.style.display = 'none';

    startWindow.hide();
});

eventEmitter.on('openStartWindow', () => {
    startWindow.show();

    windowContainer.style.display = 'flex';
});

// SETTINGS WINDOW
const settingsManager = new SettingsManager(),
    settingsWindow = new SettingsWindow(
        eventEmitter, document.getElementById('settings-window'));

eventEmitter.on('settingsChange', (args) =>
    settingsManager.set(args['key'], args['value']));

eventEmitter.on('closeSettingsWindow', () => {
    windowContainer.style.display = 'none';

    settingsWindow.hide();
});

eventEmitter.on('openSettingsWindow', () => {
    settingsWindow.updateSettings(settingsManager.settings);

    settingsWindow.show();

    windowContainer.style.display = 'flex';
});

const settingsShow = document.getElementById('settings-show');

settingsShow.addEventListener('click', () => eventEmitter.emit('openSettingsWindow'));

const exportShow = document.getElementById('export-show');

exportShow.addEventListener('click', () => eventEmitter.emit('openExportWindow'));

// DOWNLOAD WINDOW
const downloadWindow = new ExportWindow(
    eventEmitter, document.getElementById('download-window'));

eventEmitter.on('closeExportWindow', () => {
    windowContainer.style.display = 'none';

    downloadWindow.hide();

    downloadWindow.clearDownload();
});

eventEmitter.on('openExportWindow', () => {
    eventEmitter.emit('updateExportScale');

    downloadWindow.show();

    windowContainer.style.display = 'flex';
});

eventEmitter.on('updateExportScale', () => {
    const drawCanvas = editor.drawCanvas.canvas;

    downloadWindow.setScaleLabel(drawCanvas.width, drawCanvas.height);
});

eventEmitter.on('setExportDownload', () => {
    const drawCanvas = editor.drawCanvas.canvas;

    downloadWindow.setDownload(drawCanvas);
});

// COLOR PICKER
const colorManager = new ColorManager(),
    colorPicker = new ColorPicker(eventEmitter);

const colorHexExpr = /^#(([0-9A-F]{6})|([0-9A-F]{8}))$/i;

eventEmitter.on('pickerHexChange', (args) => {
    if (!colorHexExpr.test(args['hex'])) {
        return;
    }

    colorManager.hex = args['hex'];

    eventEmitter.emit('colorChange', {
        'color': colorManager.color,
        'hex': colorManager.hex,
        'hsla': colorManager.hsla,
        'hsva': colorManager.hsva,
        'rgba': colorManager.rgba,
    });
});

eventEmitter.on('pickerValueChange', (args) => {
    colorManager.color = args['color'];

    eventEmitter.emit('colorChange', {
        'color': colorManager.color,
        'hex': colorManager.hex,
        'hsla': colorManager.hsla,
        'hsva': colorManager.hsva,
        'rgba': colorManager.rgba,
    });
});

eventEmitter.on('pickerHslaChange', (args) => {
    colorManager.hsla = args['hsla'];

    eventEmitter.emit('colorChange', {
        'color': colorManager.color,
        'hex': colorManager.hex,
        'hsla': colorManager.hsla,
        'hsva': colorManager.hsva,
        'rgba': colorManager.rgba,
    });
});

eventEmitter.on('pickerHsvaChange', (args) => {
    colorManager.hsva = args['hsva'];

    eventEmitter.emit('colorChange', {
        'color': colorManager.color,
        'hex': colorManager.hex,
        'hsla': colorManager.hsla,
        'hsva': colorManager.hsva,
        'rgba': colorManager.rgba,
    });
});

let pickerListenerRemove;

eventEmitter.on('pickerInit', (args) => {
    switch(args['format']) {
        case 'hsla':
            colorPicker.getHslaValueUi();
            pickerListenerRemove = eventEmitter.on('colorChange', (args) => {
                colorPicker.setPreviewValue(args['rgba']);
                colorPicker.setHexValue(args['hex']);
                colorPicker.setSliderValues(args['hsla']);
                colorPicker.setNumberValues(args['hsla']);
            });
            break;
        case 'hsva':
            colorPicker.getHsvaValueUi();
            pickerListenerRemove = eventEmitter.on('colorChange', (args) => {
                colorPicker.setPreviewValue(args['rgba']);
                colorPicker.setHexValue(args['hex']);
                colorPicker.setSliderValues(args['hsva']);
                colorPicker.setNumberValues(args['hsva']);
            });
            break;
        case 'rgba':
            colorPicker.getRgbaValueUi();
            pickerListenerRemove = eventEmitter.on('colorChange', (args) => {
                colorPicker.setPreviewValue(args['rgba']);
                colorPicker.setHexValue(args['hex']);
                colorPicker.setSliderValues(args['color']);
                colorPicker.setNumberValues(args['color']);
            });
            break;
    }

    eventEmitter.emit('pickerValueChange', {'color': args['color']});
});

eventEmitter.on('pickerReplace', (args) => {
    colorPicker.removeFormatValueUi();

    pickerListenerRemove();

    eventEmitter.emit('pickerInit', {
        'format': args['format'],
        'color': colorManager.color,
    });
});

eventEmitter.emit('pickerInit', {
    'format': settingsManager.get('color-format'),
    'color': [255, 0, 0, 255],
});

const paletteManager = new PaletteManager(),
    palette = new Palette(eventEmitter);

eventEmitter.on('paletteAdd', () => {
    const color = colorManager.color;

    paletteManager.add(color);

    palette.add(color);
});

eventEmitter.on('paletteClear', () => {
    paletteManager.clear();

    palette.clear();
});

eventEmitter.on('palettePick', (args) => {
    const color = args['color'];

    colorManager.color = color;

    eventEmitter.emit('pickerValueChange', {'color': color});

    paletteManager.add(color);
});

// PALETTE
const paletteWindow = new PaletteWindow(
    eventEmitter, document.getElementById('palette-window'));

eventEmitter.on('openPaletteWindow', () => {
    paletteWindow.clearLocalPalettes();

    paletteWindow.addLocalPalettes(paletteManager.getLocalPalettes());

    paletteWindow.show();

    windowContainer.style.display = 'flex';
});

eventEmitter.on('closePaletteWindow', () => {
    windowContainer.style.display = 'none';

    paletteWindow.hide();
});

eventEmitter.on('paletteSaveCurrent', (args) => {
    paletteManager.saveCurrentPalette(args['name']);

    paletteManager.updateLocalPalettes();
});

eventEmitter.on('paletteLoad', (args) => {
    palette.clear();

    paletteManager.setCurrentPalette(args['name']);

    paletteManager.current.forEach((color) => palette.add(color));
});

eventEmitter.on('paletteImport', (args) => {
    paletteManager.clear();

    args['palettes'].forEach((color) => paletteManager.add(color));

    palette.clear();

    paletteManager.current.forEach((color) => palette.add(color));
});

eventEmitter.on('paletteExport', () => {
    const paletteString = JSON.stringify(paletteManager.current);

    console.log('exporting', `data:text/json,${paletteString}`);

    const exportAnchor = paletteWindow.exportAnchor;

    exportAnchor.href = `data:text/json,${paletteString}`;

    // exportAnchor.click();

    // exportAnchor.removeAttribute('href');
});

// TOOL BOX
const toolMap = {
    'bucket': BucketTool,
    'dropper': DropperTool,
    'eraser': EraserTool,
    'line': LineTool,
    'pencil': PencilTool,
    'select': SelectTool,
};

const toolBox = new ToolBox(eventEmitter, toolMap),
    toolManager = new ToolManager();

eventEmitter.on('toolPick', (args) => {
    const toolClass = args['tool'],
        toolInstance = new toolClass(eventEmitter, editor, colorManager);

    toolManager.removeCurrent();

    toolManager.setCurrent(toolInstance, mobileQuery.matches);
});

eventEmitter.emit('toolPick', {'tool': toolMap['pencil']});

let timeout;

window.addEventListener('resize', () => {
    clearTimeout(timeout);

    timeout = setTimeout(
        () => {
            editor.configure(startWindow.widthInput.value, startWindow.heightInput.value);
            editor.drawCanvas.drawLayers();
        }, 100);
});

const buttonUndo = document.getElementById('button-undo'),
    buttonRedo = document.getElementById('button-redo');

buttonUndo.addEventListener('click', () => {
    editor.drawCanvas.currentLayer.undoChange();

    editor.drawCanvas.clear();

    editor.drawCanvas.currentLayer.draw();
});

buttonRedo.addEventListener('click', () => {
    editor.drawCanvas.currentLayer.redoChange();

    editor.drawCanvas.clear();

    editor.drawCanvas.currentLayer.draw();
});
