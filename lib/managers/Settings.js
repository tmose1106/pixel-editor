const defaults = [
    ['color-format', 'hsla'],
];

export default class SettingsManager {
    constructor() {
        const settingsValues = localStorage.getItem('settings');

        this.settings = new Map(settingsValues ? JSON.parse(settingsValues) : defaults);
    }

    get(key) {
        return this.settings.get(key);
    }

    set(key, value) {
        this.settings.set(key, value);

        localStorage.setItem('settings', JSON.stringify(Array.from(this.settings)));
    }

    reset() {
        localStorage.clear('settings');
    }
}