export default class PaletteManager {
    constructor() {
        this.palettes = this.getLocalPalettes();

        this.current = [];
    }

    getLocalPalettes() {
        const localPalettes = localStorage.getItem('palettes');

        return (localPalettes ? JSON.parse(localPalettes) : {});
    }

    updateLocalPalettes() {
        localStorage.setItem('palettes', JSON.stringify(this.palettes));
    }

    saveCurrentPalette(name) {
        this.palettes[name] = Array.from(this.current);

        this.updateLocalPalettes();
    }

    setCurrentPalette(name) {
        const selectedPalette = this.palettes[name];

        if (selectedPalette) {
            this.current = Array.from(selectedPalette);
        } else {
            console.log('palette does not exist');
        }
    }

    add(color) {
        this.current.push(color);
    }

    clear() {
        this.current = [];
    }
}