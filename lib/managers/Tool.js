export default class ToolManager {
    constructor() {
        this.current = null;
    }

    setCurrent(toolInstance, mobileHandlers) {
        toolInstance.addHandlers(
            mobileHandlers
            ? toolInstance.getTouchHandlerMap()
            : toolInstance.getMouseHandlerMap()
        );

        this.current = toolInstance;
    }

    removeCurrent() {
        if (!this.current) {
            return;
        }

        this.current.removeHandlers();

        if (this.current.extraUi) {
            this.current.extraUi.style.setProperty('display', 'none');
        }

        this.current = null;
    }
}