export default class ColorManager {
    constructor(initialColor) {
        this._color = initialColor;
    }

    static getHexFromRgba(r, g, b, a) {
        const valueToHex = (value) => parseInt(value).toString(16).padStart(2, '0');

        if (a && a < 255) {
            return `#${valueToHex(r)}${valueToHex(g)}${valueToHex(b)}${valueToHex(a)}`;
        } else {
            return `#${valueToHex(r)}${valueToHex(g)}${valueToHex(b)}`;
        }
    }

    static getHslaFromRgba(rgba) {
        const r = rgba[0] / 255;
        const g = rgba[1] / 255;
        const b = rgba[2] / 255;

        const min = Math.min(r, g, b);
        const max = Math.max(r, g, b);
        const delta = max - min;

        let h;
        let s;

        if (max === min) {
            h = 0;
        } else if (r === max) {
            h = (g - b) / delta;
        } else if (g === max) {
            h = 2 + (b - r) / delta;
        } else if (b === max) {
            h = 4 + (r - g) / delta;
        }

        h = Math.min(h * 60, 360);

        if (h < 0) {
            h += 360;
        }

        const l = (min + max) / 2;

        if (max === min) {
            s = 0;
        } else if (l <= 0.5) {
            s = delta / (max + min);
        } else {
            s = delta / (2 - max - min);
        }

        return [h, s * 100, l * 100, rgba[3]];
    }

    static getHsvaFromRgba(rgb) {
        let rdif;
        let gdif;
        let bdif;
        let h;
        let s;

        const r = rgb[0] / 255;
        const g = rgb[1] / 255;
        const b = rgb[2] / 255;

        const v = Math.max(r, g, b);
        const diff = v - Math.min(r, g, b);
        const diffc = function (c) {
            return (v - c) / 6 / diff + 1 / 2;
        };

        if (diff === 0) {
            h = 0;
            s = 0;
        } else {
            s = diff / v;
            rdif = diffc(r);
            gdif = diffc(g);
            bdif = diffc(b);

            if (r === v) {
                h = bdif - gdif;
            } else if (g === v) {
                h = (1 / 3) + rdif - bdif;
            } else if (b === v) {
                h = (2 / 3) + gdif - rdif;
            }

            if (h < 0) {
                h += 1;
            } else if (h > 1) {
                h -= 1;
            }
        }

        return [
            h * 360,
            s * 100,
            v * 100,
            rgb[3]
        ];
    }

    static getRgbaFromHsla(hsl) {
        const h = hsl[0] / 360;
        const s = hsl[1] / 100;
        const l = hsl[2] / 100;
        let t2;
        let t3;
        let val;

        if (s === 0) {
            val = l * 255;
            return [val, val, val, hsl[3]];
        }

        if (l < 0.5) {
            t2 = l * (1 + s);
        } else {
            t2 = l + s - l * s;
        }

        const t1 = 2 * l - t2;

        const rgb = [0, 0, 0, hsl[3]];
        for (let i = 0; i < 3; i++) {
            t3 = h + 1 / 3 * -(i - 1);
            if (t3 < 0) {
                t3++;
            }

            if (t3 > 1) {
                t3--;
            }

            if (6 * t3 < 1) {
                val = t1 + (t2 - t1) * 6 * t3;
            } else if (2 * t3 < 1) {
                val = t2;
            } else if (3 * t3 < 2) {
                val = t1 + (t2 - t1) * (2 / 3 - t3) * 6;
            } else {
                val = t1;
            }

            rgb[i] = val * 255;
        }

        return rgb;
    }

    static getRgbaFromHsva(hsv) {
        const h = hsv[0] / 60;
        const s = hsv[1] / 100;
        let v = hsv[2] / 100;
        const hi = Math.floor(h) % 6;

        const f = h - Math.floor(h);
        const p = 255 * v * (1 - s);
        const q = 255 * v * (1 - (s * f));
        const t = 255 * v * (1 - (s * (1 - f)));
        v *= 255;

        switch (hi) {
            case 0:
                return [v, t, p, hsv[3]];
            case 1:
                return [q, v, p, hsv[3]];
            case 2:
                return [p, v, t, hsv[3]];
            case 3:
                return [p, q, v, hsv[3]];
            case 4:
                return [t, p, v, hsv[3]];
            case 5:
                return [v, p, q, hsv[3]];
        }
    }

    static getRgbaString(r, g, b, a) {
        return `rgba(${r}, ${g}, ${b}, ${a / 255})`
    }

    static parseHex(hexString) {
        const normalPattern = /^#([0-9A-F]{6})$/i,
            alphaPattern = /^#([0-9A-F]{8})$/i;

        if (normalPattern.test(hexString)) {
            return [
                parseInt(hexString.substr(1, 2), 16),
                parseInt(hexString.substr(3, 2), 16),
                parseInt(hexString.substr(5, 2), 16),
                255
            ];
        } else if (alphaPattern.test(hexString)) {
            return [
                parseInt(hexString.substr(1, 2), 16),
                parseInt(hexString.substr(3, 2), 16),
                parseInt(hexString.substr(5, 2), 16),
                parseInt(hexString.substr(7, 2), 16)
            ];
        } else {
            return null;
        }
    }

    get color() {
        return this._color.map((value) => Math.round(value));
    }

    set color(value) {
        this._color = value.map((value) => parseInt(value));
    }

    get hex() {
        return ColorManager.getHexFromRgba(...this.color);
    }

    set hex(hexValue) {
        this._color = ColorManager.parseHex(hexValue);
    }

    get hsla() {
        return ColorManager.getHslaFromRgba(this.color).map((value) => Math.round(value));
    }

    set hsla(value) {
        this._color = ColorManager.getRgbaFromHsla(value.map((value) => parseInt(value)));
    }

    get hsva() {
        return ColorManager.getHsvaFromRgba(this.color).map((value) => Math.round(value));
    }

    set hsva(value) {
        this._color = ColorManager.getRgbaFromHsva(value.map((value) => parseInt(value)));
    }

    get rgba() {
        return ColorManager.getRgbaString(...this.color);
    }
}