// import { ColorSelect } from './picker.js';

import { BackgroundCanvas, DrawCanvas, ToolCanvas } from './canvas.js';

export default class Editor {
    constructor() {
        this.backgroundCanvas = new BackgroundCanvas(
            document.getElementById('background'), './transparent.png');

        this.drawCanvas = new DrawCanvas(document.getElementById('game'));

        this.toolCanvas = new ToolCanvas(document.getElementById('highlight'));
    }

    resize() {
        const contextWidth = this.width,
            contextHeight = this.height;

        const windowWidth = window.innerWidth,
            windowHeight = window.innerHeight;

        const widthScale = Math.floor(windowWidth / contextWidth),
            heightScale = Math.floor(windowHeight / contextHeight),
            maxScale = Math.floor(640 / contextHeight),
            canvasScale = Math.min(widthScale, heightScale, maxScale);

        const container = document.getElementById('editor-container');

        container.style.setProperty('width', `${contextWidth * canvasScale}px`);
        container.style.setProperty('height', `${contextHeight * canvasScale}px`);

        this.drawCanvas.reconfigure(canvasScale);

        this.toolCanvas.canvas.width = contextWidth * canvasScale;
        this.toolCanvas.canvas.height = contextHeight * canvasScale;
        this.toolCanvas.reconfigure(1);
    }

    configure(contextWidth, contextHeight) {
        const windowWidth = window.innerWidth,
            windowHeight = window.innerHeight;

        const widthScale = Math.floor(windowWidth / contextWidth),
            heightScale = Math.floor(windowHeight / contextHeight),
            maxScale = Math.floor(640 / contextHeight),
            canvasScale = Math.min(widthScale, heightScale, maxScale);

        const container = document.getElementById('editor-container');

        container.style.setProperty('width', `${contextWidth * canvasScale}px`);
        container.style.setProperty('height', `${contextHeight * canvasScale}px`);

        this.drawCanvas.canvas.width = contextWidth;
        this.drawCanvas.canvas.height = contextHeight;
        this.drawCanvas.reconfigure(canvasScale);

        this.toolCanvas.canvas.width = contextWidth * canvasScale;
        this.toolCanvas.canvas.height = contextHeight * canvasScale;
        this.toolCanvas.reconfigure(1);

        this.backgroundCanvas.canvas.width = contextWidth;
        this.backgroundCanvas.canvas.height = contextHeight;
        this.backgroundCanvas.reconfigure(canvasScale);
    }
}