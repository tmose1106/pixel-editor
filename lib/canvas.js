import Layer from './layer.js';

class ScalableCanvas {
    constructor(canvas) {
        this.canvas = canvas;
        this.context = canvas.getContext('2d');

        this.reconfigure(1);
    }

    reconfigure(scale) {
        this.scale = scale;

        this.dimensions = this.canvas.getBoundingClientRect();
    }

    clear() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    clearCoords(coordinates) {
        coordinates.forEach(([x, y]) => this.context.clearRect(x, y, 1, 1));
    }

    fillCoords(coordinates, color) {
        this.fillScaledCoords(coordinates, color, 1);
    }

    fillScaledCoords(coordinates, color, scale) {
        this.context.fillStyle = color;

        this.context.beginPath();

        coordinates.forEach(
            ([x, y]) => this.context.rect(x * scale, y * scale, scale, scale));

        this.context.fill();
    }

    getFullCoordinates(x, y) {
        const boundRect = this.canvas.getBoundingClientRect();

        return [x - boundRect.left, y + window.pageYOffset - boundRect.y];
    }

    getScaledCoordinate(x, y, scale) {
        return this.getFullCoordinates(x, y)
            .map((value) => Math.ceil(value / scale) - 1);
    }
}

class DrawCanvas extends ScalableCanvas {
    constructor(canvas) {
        super(canvas);

        this.layers = [];

        this.addLayer();
    }

    static mapCoordinate(x, y, factor) {
        return [x, y].map((value) => Math.ceil(value / factor) - 1);
    }

    getCoordinates(x, y) {
        const boundRect = this.dimensions;

        return DrawCanvas.mapCoordinate(
            x - boundRect.left, y + window.pageYOffset - boundRect.y, this.scale);
    }

    addLayer() {
        const layer = new Layer(this);

        this.layers.push(layer);

        this.currentLayer = layer;
    }

    drawLayers() {
        this.clear();

        this.layers.reverse().forEach((layer) => layer.draw());
    }
}

class BackgroundCanvas extends ScalableCanvas {
    constructor(canvas, backgroundSource) {
        super(canvas);
    }

    reconfigure(scale) {
        this.scale = scale;

        this.dimensions = this.canvas.getBoundingClientRect();

        this.size = this.dimensions.width;

        this.drawBackground();
    }

    drawBackground() {
        const width = this.canvas.width,
            height = this.canvas.height;

        this.context.fillStyle = 'rgb(143, 143, 143)';

        this.context.fillRect(0, 0, width, height);

        this.context.beginPath();

        this.context.fillStyle = 'rgb(111, 111, 111)';

        for (let i = 0; i < height; ++i) {
            for (let j = 0; j < width; ++j) {
                this.context.rect(2 * j + (i % 2 ? 0 : 1), i, 1, 1);
            }
        }

        this.context.fill();
    }
}

class ToolCanvas extends ScalableCanvas {
    constructor(canvas) {
        super(canvas);
    }
}

export { BackgroundCanvas, DrawCanvas, ToolCanvas };