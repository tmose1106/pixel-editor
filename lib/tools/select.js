import Tool from './base.js';

export default class SelectTool extends Tool {
    constructor(eventManager, editor, picker) {
        super(eventManager, editor, picker);

        this.selection = null;
    }

    getMouseHandlerMap() {
        return {
            'mousedown': (event) => {
                if (this.selection) {
                    this.startMove(event.clientX, event.clientY);
                } else {
                    this.startSelect(event.clientX, event.clientY);
                }
            },
            'mousemove': (event) => {
                if (this.selection && this.startPixel) {
                    this.continueMove(event.clientX, event.clientY);
                } else {
                    this.continueSelect(event.clientX, event.clientY);
                }
            },
            'mouseup': (event) => {
                if (this.selection) {
                    this.endMove(event.clientX, event.clientY);
                } else {
                    this.endSelect(event.clientX, event.clientY);
                }
            },
        };
    }

    getTouchHandlerMap() {
        return {
            'touchstart': (event) => {
                const touch = event.touches[0];

                this.startSelect(touch.clientX, touch.clientY);
            },
            'touchmove': (event) => {
                const touch = event.touches[0];

                this.continueSelect(touch.clientX, touch.clientY);
            },
            'touchend': (event) => {
                const touch = event.changedTouches[0];

                this.endSelect(touch.clientX, touch.clientY);
            },
        };
    }

    static coordinatesEqual(coord1, coord2) {
        return coord1[0] === coord2[0] && coord1[1] === coord2[1];
    }

    static coordinatesDifference(coord1, coord2) {
        return [coord1[0] - coord2[0], coord1[1] - coord2[1]];
    }

    static pointInArea(x, y, xa, ya, wa, ha) {
        const dx = x - xa, dy = y - ya;

        return (dx >= 0 && dy >= 0 && (dx < wa) && (dy < ha));
    }

    startSelect(x, y) {
        this.startPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale);

        this.previousPixel = this.startPixel;
    }

    continueSelect(x, y) {
        if (!this.startPixel) {
            return;
        }

        const startPixel = this.startPixel,
            currentPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale);

        if (SelectTool.coordinatesEqual(currentPixel, this.previousPixel)) {
            return;
        } else {
            this.previousPixel = currentPixel;
        }

        this.toolCanvas.clear();

        this.drawSelection(...this.getSelection(...startPixel, ...currentPixel));
    }

    endSelect(x, y) {
        const startPixel = this.startPixel,
            endPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale);

        this.startPixel = undefined;

        if (SelectTool.coordinatesEqual(startPixel, endPixel)) {
            return;
        }

        this.toolCanvas.clear();

        const selection = this.getSelection(...startPixel, ...endPixel);

        this.drawSelection(...selection);

        this.selection = {
            'area': selection,
            'start': startPixel,
            'end': endPixel,
            'data': this.drawCanvas.context.getImageData(...selection),
        };

        this.firstMove = true;
    }

    startMove(x, y) {
        this.startPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale);

        if (this.firstMove) {
            this.moveStart = this.selection.start;

            console.log('starting move', this.moveStart);
        }

        if (SelectTool.pointInArea(...this.startPixel, ...this.selection['area'])) {
            console.log(this.selection['data']);
        } else {

            this.toolCanvas.clear();
            this.selection = null; // Exit move mode
        }
    }

    continueMove(x, y) {
        const currentPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale);

        if (this.previousPixel && SelectTool.coordinatesEqual(currentPixel, this.previousPixel)) {
            return;
        } else {
            this.previousPixel = currentPixel;
        }

        const newStartPixel = SelectTool.coordinatesDifference(
            this.selection['area'].slice(0, 2),
            SelectTool.coordinatesDifference(this.startPixel, currentPixel)
        );

        this.drawCanvas.clear();

        this.drawCanvas.drawLayers();

        this.drawCanvas.context.clearRect(...this.moveStart, this.selection.data.width, this.selection.data.height);

        this.drawCanvas.context.putImageData(this.selection['data'], ...newStartPixel);

        this.toolCanvas.clear();

        this.drawSelection(...newStartPixel, ...this.selection['area'].slice(2, 4));
    }

    endMove(x, y) {
        const endPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale);

        if (SelectTool.coordinatesEqual(endPixel, this.startPixel)) {
            this.finishMove();
        } else {
            const newStartPixel = SelectTool.coordinatesDifference(
                this.selection['area'].slice(0, 2),
                SelectTool.coordinatesDifference(this.startPixel, endPixel)
            );

            this.selection['area'] = [
                ...newStartPixel,
                ...this.selection['area'].slice(2, 4)
            ];

            this.firstMove = false;
        }

        this.startPixel = null;
    }

    finishMove() {
        console.log('finishing move', this.moveStart);

        this.drawCanvas.currentLayer.addChange(
            'move',
            null,
            {
                'oldStart': this.moveStart,
                'newStart': this.selection.area.slice(0, 2),
                'data': this.selection.data,
            }
        );

        this.drawCanvas.currentLayer.clearUndone();

        this.drawCanvas.drawLayers();

        this.toolCanvas.clear();

        this.selection = null;
    }

    getSelection(x1, y1, x2, y2) {
        const dx = x2 - x1,
            dy = y2 - y1;

        if (dx >= 0 && dy >= 0) {
            return [x1, y1, dx + 1, dy + 1];
        } else if (dx < 0 && dy < 0) {
            return [x2, y2, -dx, -dy];
        } else if (dx < 0 && dy >= 0) {
            return [x2, y1, -dx, dy + 1];
        } else {
            return [x1, y2, dx + 1, -dy];
        }
    }

    drawSelection(x, y, w, h) {
        const ctx = this.toolCanvas.context,
            scale = this.drawCanvas.scale,
            [sx, sy, sw, sh] = [x, y, w, h].map((value) => value * scale);

        ctx.fillStyle = 'rgba(128, 128, 128, 0.5)';
        ctx.fillRect(sx, sy, sw, sh);

        ctx.setLineDash([8]);
        ctx.lineWidth = 4;
        ctx.strokeStyle = 'cyan';
        ctx.strokeRect(sx, sy, sw, sh);

        ctx.setLineDash([]);
        ctx.fillStyle = 'cyan';

        // for (const [x, y] of [[sx, sy], [sx + sw, sy], [sx, sy + sh], [sx + sw, sy + sh]]) {
        //     ctx.beginPath();
        //     ctx.arc(x, y, 14, 0, 2 * Math.PI);
        //     ctx.stroke();
        //     ctx.fill();
        // }
    }

    getExtendedUi() {
        const container = document.createElement('div');

        const buttonCallbackMap = {
            'move': () => {},
            'copy': () => {},
            'paste': () => {},
        };

        Object.entries(buttonCallbackMap).forEach(([name, callback]) => {
            const button = document.createElement('button');

            button.appendChild(document.createTextNode(name));

            button.addEventListener('click', callback);

            container.appendChild(button);
        });

        return container;
    }
}