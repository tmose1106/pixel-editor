export default class Tool {
    constructor(eventManager, editor, picker) {
        this.manager = eventManager;

        // this.editor = editor;

        this.drawCanvas = editor.drawCanvas;
        this.toolCanvas = editor.toolCanvas;

        this.picker = picker;

        this.addedHandlers = new Set();

        this.extraUi = this.getExtendedUi();
    }

    addHandler(eventName, handlerMethod) {
        this.addedHandlers.add([eventName, handlerMethod]);

        this.drawCanvas.canvas.addEventListener(eventName, handlerMethod);
    }

    addHandlers(handlerMap) {
        Object.entries(handlerMap).forEach(
            ([eventName, handler]) => this.addHandler(eventName, handler));
    }

    removeHandler(eventName, handlerMethod) {
        this.drawCanvas.canvas.removeEventListener(eventName, handlerMethod);
    }

    removeHandlers() {
        this.addedHandlers.forEach(
            ([eventName, handler]) => this.removeHandler(eventName, handler));

        this.addedHandlers.clear();
    }

    getExtendedUi() {
        return null;
    }

    /* Temporarily draw on the tool canvas */
    tempDraw(coordinates) {
        this.toolCanvas.clear();

        this.toolCanvas.context.fillStyle = this.picker.rgba;

        const scale = this.drawCanvas.scale;

        coordinates.forEach(([x, y]) =>
            this.toolCanvas.context.fillRect(x * scale, y * scale, scale, scale));
    }
}