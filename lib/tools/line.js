import Tool from './base.js';

export default class LineTool extends Tool {
    constructor(eventManager, editor, picker) {
        super(eventManager, editor, picker);

        this.drawEnabled = false;
    }

    getMouseHandlerMap() {
        return {
            'mousedown': (event) => {
                this.startLine(event.clientX, event.clientY);
            },
            'mousemove': (event) => {
                this.continueLine(event.clientX, event.clientY);
            },
            'mouseup': (event) => {
                this.endLine(event.clientX, event.clientY);
            }
        }
    }

    getTouchHandlerMap() {
        return {
            'touchstart': (event) => {
                const touch = event.touches[0];

                this.startLine(touch.clientX, touch.clientY);
            },
            'touchmove': (event) => {
                const touch = event.touches[0];

                this.continueLine(touch.clientX, touch.clientY);
            },
            'touchend': (event) => {
                const touch = event.changedTouches[0];

                this.endLine(touch.clientX, touch.clientY);
            },
        };
    }

    startLine(x, y) {
        this.startPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale);

        this.drawEnabled = true;
    }

    continueLine(x, y) {
        if (!this.drawEnabled) {
            return;
        }

        const startPixel = this.startPixel,
            currentPixel = this.toolCanvas.getScaledCoordinate(x, y, this.drawCanvas.scale),
            lineCoordinates = LineTool.getPointsBetween(...startPixel, ...currentPixel);

        this.toolCanvas.clear();

        this.toolCanvas.fillScaledCoords(lineCoordinates, this.picker.rgba, this.drawCanvas.scale);
    }

    endLine(x, y) {
        const startPixel = this.startPixel,
            endPixel = this.drawCanvas.getCoordinates(x, y);

        this.drawEnabled = false;

        this.toolCanvas.clear();

        const points = LineTool.getPointsBetween(...startPixel, ...endPixel);

        this.drawCanvas.currentLayer.addChange(
            'paint', points, { 'color': this.picker.rgba });

        this.drawCanvas.currentLayer.clearUndone();

        this.drawCanvas.drawLayers();
    }

    /* Thanks to this question here
     * https://math.stackexchange.com/questions/1918743/how-to-interpolate-points-between-2-points
     */
    static getPointsBetween(x1, y1, x2, y2) {
        const pointSet = new Set();

        const d = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));

        const dx = x2 - x1, dy = y2 - y1;

        for (let i = 0; i < Math.round(d) + 1; i++) {
            const pd = i / d,
                x = x1 + pd * dx,
                y = y1 + pd * dy;

            pointSet.add([Math.round(x), Math.round(y)]);
        }

        return pointSet;
    }
}