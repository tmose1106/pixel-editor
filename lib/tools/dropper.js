import Tool from './base.js';

export default class DropperTool extends Tool {
    getMouseHandlerMap() {
        return {
            'click': (event) => {
                const [x, y] = this.drawCanvas.getCoordinates(event.clientX, event.clientY);

                const pixel = this.drawCanvas.context.getImageData(x, y, 1, 1);

                this.manager.emit('pickerValueChange', {'color': pixel.data});
            },
        }
    }

    getTouchHandlerMap() {
        return this.getMouseHandlerMap();
    }
}