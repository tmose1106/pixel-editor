import PencilTool from './pencil.js';

export default class EraserTool extends PencilTool {
    constructor(eventManager, editor, picker) {
        super(eventManager, editor, picker);
    }

    startDraw(x, y) {
        this.coordinates.push([x, y]);

        this.drawCanvas.clearPoint(x, y);
    }

    continueDraw(x, y) {
        this.coordinates.push([x, y]);

        this.drawCanvas.clear();

        this.drawCanvas.drawLayers();

        this.drawCanvas.clearCoords(this.coordinates);
    }

    endDraw() {
        this.drawCanvas.currentLayer.addChange(
            'erase',
            this.coordinates,
        );

        this.drawCanvas.currentLayer.clearUndone();

        this.coordinates = [];
    }
}