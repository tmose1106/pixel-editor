import Tool from './base.js';

export default class PencilTool extends Tool {
    constructor(eventManager, editor, picker) {
        super(eventManager, editor, picker);

        this.coordinates = [];

        this.previousCoordinate = { x: -1, y: -1 };

        this.enableDraw = false;
    }

    getMouseHandlerMap() {
        return {
            'mousedown': (event) => {
                this.enableDraw = true;

                const [x, y] = this.drawCanvas.getCoordinates(event.clientX, event.clientY);

                this.continueDraw(x, y);
            },
            'mousemove': (event) => {
                const [x, y] = this.drawCanvas.getCoordinates(event.clientX, event.clientY),
                    prev = this.previousCoordinate;

                if ((x === prev.x && y === prev.y)) {
                    return;
                } else {
                    this.previousCoordinate = { x: x, y: y };
                }

                if (this.enableDraw) {
                    this.continueDraw(x, y);
                }
            },
            'mouseup': () => {
                this.enableDraw = false;

                this.endDraw();
            },
            // 'mouseleave': () => {
            //     this.drawCanvas.clear();

            //     this.drawCanvas.currentLayer.draw();
            // },
        };
    }

    getTouchHandlerMap() {
        const draw = (event) => {
            const touch = event.touches[0],
                [x, y] = this.drawCanvas.getCoordinates(touch.clientX, touch.clientY);

            this.continueDraw(x, y);
        };

        return {
            'touchmove': draw,
            'touchstart': draw,
            'touchend': () => this.endDraw(),
        };
    }

    continueDraw(x, y) {
        this.coordinates.push([x, y]);

        this.toolCanvas.clear();

        this.toolCanvas.fillScaledCoords(
            this.coordinates, this.picker.rgba, this.drawCanvas.scale);
    }

    endDraw() {
        this.toolCanvas.clear();

        this.drawCanvas.currentLayer.addChange(
            'paint',
            this.coordinates,
            {'color': this.picker.rgba}
        );

        this.drawCanvas.currentLayer.clearUndone();

        this.drawCanvas.drawLayers();

        this.coordinates = [];
    }
}