import Tool from './base.js';

export default class BucketTool extends Tool {
    getMouseHandlerMap() {
        return {
            'click': (event) => {
                const [x, y] = this.drawCanvas.getCoordinates(event.clientX, event.clientY);

                const imageData = this.drawCanvas.context.getImageData(
                    0, 0, this.drawCanvas.canvas.width, this.drawCanvas.canvas.height);

                const color = BucketTool.getColorAtPoint(imageData, x, y);

                const points = this.floodFillPoints(imageData, x, y, color, this.picker.color);

                this.drawCanvas.currentLayer.addChange(
                    'paint', points, { 'color': this.picker.rgba });

                this.drawCanvas.currentLayer.draw();

                this.drawCanvas.currentLayer.clearUndone();
            },
        }
    }

    getTouchHandlerMap() {
        return this.getMouseHandlerMap();
    }

    /* Use a variation of the flood fill algorithm found on Wikipedia */
    floodFillPoints(imageData, x, y, targetColor, replacementColor) {
        if (targetColor === replacementColor) {
            return;
        }

        const nodeColor = BucketTool.getColorAtPoint(imageData, x, y);

        if (!nodeColor || !BucketTool.colorsEqual(nodeColor, targetColor)) {
            return;
        }

        const maxIndex = (imageData.data.length / 4) - 1;

        const indexSet = new Set();

        const startIndex = BucketTool.getIndex(x, y, imageData.width, imageData.height);

        indexSet.add(startIndex);

        const q = new Queue();

        q.enqueue(startIndex);

        while (!q.isEmpty) {
            const n = q.dequeue();

            const surrounding = BucketTool.getSurroundingIndices(n, imageData.width, imageData.height);

            for (const index of surrounding) {
                if (index < 0 || index > maxIndex) {
                    continue;
                }

                const otherColor = imageData.data.slice(index * 4, (index + 1) * 4);

                if (!(indexSet.has(index)) && BucketTool.colorsEqual(otherColor, targetColor)) {
                    indexSet.add(index);
                    q.enqueue(index);
                }
            }
        }

        return Array.from(indexSet).map((value) =>
            [value % imageData.width, Math.floor(value / imageData.width)]);
    }

    static getSurroundingPoints(x, y) {
        return [
            [x - 1, y],
            [x, y - 1],
            [x + 1, y],
            [x, y + 1],
        ];
    }

    static getSurroundingIndices(i, w, h) {
        return [
            i - 1,
            i - w,
            i + 1,
            i + w,
        ];
    }

    static getIndex(x, y, width, height) {
        if (x < 0 || x >= width || y < 0 || y >= height) {
            return undefined;
        }

        return y * width + x;
    }

    static getColorAtPoint(imageData, x, y) {
        const i = BucketTool.getIndex(x, y, imageData.width, imageData.height);

        if (i === undefined) {
            return undefined;
        }

        const colorIndex = i * 4,
            colorData = imageData.data.slice(colorIndex, colorIndex + 4);

        return colorData;
    }

    static colorsEqual(color1, color2) {
        return color1.every((value, index) => color2[index] === value);
    }
}

class Queue {
    constructor() {
        this.queue = [];
        this.offset = 0;
    }

    get length() {
        return this.queue.length - this.offset;
    }

    get isEmpty() {
        return this.queue.length === 0;
    }

    enqueue(value) {
        this.queue.push(value);
    }

    dequeue() {
        if (this.isEmpty) {
            return undefined;
        }

        const value = this.queue[this.offset];

        if (++this.offset * 2 >= this.queue.length) {
            this.queue = this.queue.slice(this.offset);

            this.offset = 0;
        }

        return value;
    }

    peek() {
        return (this.length > 0 ? this.queue[this.offset] : undefined);
    }
}