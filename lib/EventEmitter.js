export default class EventEmitter {
    constructor() {
        this.events = new Map();
    }

    on(event, listener) {
        if (!this.events.has(event)) {
            this.events.set(event, []);
        }

        this.events.get(event).push(listener);

        return () => this.removeListener(event, listener);
    }

    removeListener(event, listener) {
        if (this.events.has(event)) {
            const eventListeners = this.events.get(event);

            const index = eventListeners.indexOf(listener);

            if (index > -1) {
                eventListeners.splice(index, 1);
            }
        }
    }

    emit(event, ...args) {
        if (this.events.has(event)) {
            this.events.get(event).forEach(
                (listener) => listener.apply(this, args));
        } else {
            console.log(`event ${event} doesn't have subscribers`);
        }
    }

    // once(event, listener) {
    //     const remove = this.on(event, (...args) => {
    //         remove();
    //         listener.apply(this, args);
    //     });
    // }
};