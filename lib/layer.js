export default class Layer {
    constructor(canvas, imageSource, loadCallback) {
        this.canvas = canvas.canvas;
        this.context = canvas.context;

        this.changes = [];
        this.reverted = [];

        this.image = new Image();

        if (imageSource && loadCallback) {
            this.image.addEventListener('load', loadCallback);

            this.image.src = imageSource;
        }
    }

    addChange(type, coordinates, extra) {
        this.changes.push({
            type: type,
            coordinates: coordinates,
            ...extra,
        });
    }

    undoChange() {
        const change = this.changes.pop()

        if (!change) return;

        this.reverted.push(change);
    }

    clearUndone() {
        this.reverted = [];
    }

    redoChange() {
        const change = this.reverted.pop();

        if (!change) return;

        this.changes.push(change);
    }

    draw() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.context.drawImage(this.image, 0, 0);

        if (this.changes.length === 0) {
            return;
        }

        this.changes.forEach((change) => {
            switch(change.type) {
                case 'erase':
                    change.coordinates.forEach(
                        (coordinate) => this.context.clearRect(...coordinate, 1, 1));

                    break;
                case 'paint':
                    this.context.strokeStyle = change.color;
                    this.context.fillStyle = change.color;

                    change.coordinates.forEach(
                        (coordinate) => this.context.fillRect(...coordinate, 1, 1));

                    break;
                case 'move':
                    const data = change.data;

                    this.context.clearRect(...change.oldStart, data.width, data.height);
                case 'copy':
                    this.context.putImageData(change.data, ...change.newStart)

                    break;
            }
        });
    }
}