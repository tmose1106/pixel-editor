import BaseWindow from './BaseWindow.js';

export default class ExportWindow extends BaseWindow {
    constructor(eventManager, container) {
        super(eventManager, container);

        const exportClose = document.getElementById('export-window-close');

        exportClose.addEventListener('click', () => eventManager.emit('closeExportWindow'));

        const exportName = document.getElementById('export-name');

        exportName.addEventListener('input', () => this.clearDownload());

        this.exportName = exportName;

        const exportScale = document.getElementById('export-scale');

        exportScale.addEventListener('change', () => {
            this.clearDownload();

            eventManager.emit('updateExportScale');
        });

        this.exportScale = exportScale;

        const exportLink = document.getElementById('export-download');

        this.exportLink = exportLink;

        const exportButton = document.getElementById('export-process');

        exportButton.addEventListener(
            'click', () => eventManager.emit('setExportDownload'));

        window.addEventListener('load', () => {
            exportScale.value = exportScale.min;
        });
    }

    static scaleCanvas(canvas, scale) {
        const resizeCanvas = document.createElement('canvas'),
            resizeContext = resizeCanvas.getContext('2d');

        resizeCanvas.width = canvas.width * scale;
        resizeCanvas.height = canvas.height * scale;

        resizeContext.imageSmoothingEnabled = false;

        resizeContext.scale(scale, scale);

        resizeContext.drawImage(canvas, 0, 0);

        return resizeCanvas.toDataURL();
    }

    setScaleLabel(width, height) {
        const scaleLabel = document.getElementById('export-dimensions'),
            scaleValue = document.getElementById('export-scale').value;

        scaleLabel.innerText = `${width * scaleValue}x${height * scaleValue}`;
    }

    setDownload(data) {
        const downloadLink = this.exportLink;

        downloadLink.download = `${this.exportName.value}.png`;

        downloadLink.href = ExportWindow.scaleCanvas(data, this.exportScale.value);
    }

    clearDownload() {
        const downloadLink = this.exportLink;

        downloadLink.removeAttribute('download');
        downloadLink.removeAttribute('href');
    }
}