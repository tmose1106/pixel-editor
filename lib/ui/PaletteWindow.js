import BaseWindow from './BaseWindow.js';

export default class PaletteWindow extends BaseWindow {
    constructor(eventManager, container) {
        super(eventManager, container);

        const paletteCloseButton = document.getElementById('palette-window-close');

        paletteCloseButton.addEventListener('click', () => this.manager.emit('closePaletteWindow'));

        const paletteSaveInput = document.getElementById('palette-save-name'),
            paletteSaveButton = document.getElementById('palette-save-button');

        paletteSaveInput.value = '';

        paletteSaveButton.addEventListener('click', () => {
            const paletteName = paletteSaveInput.value;

            if (paletteName.length > 0) {
                this.manager.emit('paletteSaveCurrent', {'name': paletteName});
            }
        });

        const loadSelect = this.loadSelect = document.getElementById('palette-load-select');

        const paletteLoadButton = document.getElementById('palette-load-button');

        paletteLoadButton.addEventListener('click', () => {
            const selectedPaletteName = loadSelect.options[loadSelect.selectedIndex].value;

            this.manager.emit('paletteLoad', {'name': selectedPaletteName});

            this.manager.emit('closePaletteWindow');
        });

        const paletteImportInput = document.getElementById('palette-import-input'),
            paletteImportButton = document.getElementById('palette-import-button');

        paletteImportInput.value = '';

        paletteImportButton.addEventListener('click', () => {
            const files = paletteImportInput.files;

            files[0].text().then(
                (value) => {
                    this.manager.emit('paletteImport', {'palettes': JSON.parse(value)});
                    this.manager.emit('closePaletteWindow');
                },
                () => console.log('failed loading file')
            );
        });

        const paletteExportButton = document.getElementById('palette-export-button');

        paletteExportButton.addEventListener('click', (event) => {
            this.manager.emit('paletteExport');
        });

        const paletteExportAnchor = this.exportAnchor = document.getElementById('palette-export-anchor');

        paletteExportAnchor.addEventListener(
            'click', () => paletteExportAnchor.removeAttribute('href'));
    }

    addLocalPalettes(palettes) {
        const paletteNames = Object.keys(palettes);

        if (paletteNames.length === 0) {
            this.loadSelect.disabled = true;
            return;
        }

        const container = this.loadSelect;

        paletteNames.forEach((paletteName) => {
            const paletteOption = document.createElement('option');

            paletteOption.value = paletteName;

            paletteOption.appendChild(document.createTextNode(paletteName));

            container.appendChild(paletteOption);
        });

        container.disabled = false;
    }

    clearLocalPalettes() {
        const container = this.loadSelect;

        while (container.lastChild) {
            container.removeChild(container.lastChild);
        }

        container.disabled = true;
    }
}