export default class ColorPicker {
    constructor(eventManager) {
        this.eventManager = eventManager;

        this.preview = document.getElementById('picker-preview');

        this.hex = document.getElementById('picker-hex');

        this.hex.addEventListener('input', () => {
            this.eventManager.emit('pickerHexChange', {'hex': this.hex.value});
        });

        this.inputs = document.getElementById('picker-inputs');
    }

    static getInputElement(type, min, max) {
        const element = document.createElement('input');

        element.type = type;
        element.min = min;
        element.max = max;

        return element;
    }

    getRgbaValueUi() {
        this.getFormatValueUi(
            [['red', 255], ['green', 255], ['blue', 255], ['alpha', 255]],
            (values) => this.eventManager.emit('pickerValueChange', {'color': values})
        );
    }

    getHslaValueUi() {
        this.getFormatValueUi(
            [['hue', 359], ['satur', 100], ['light', 100], ['alpha', 255]],
            (values) => this.eventManager.emit('pickerHslaChange', {'hsla': values})
        );
    }

    getHsvaValueUi() {
        this.getFormatValueUi(
            [['hue', 359], ['satur', 100], ['value', 100], ['alpha', 255]],
            (values) => this.eventManager.emit('pickerHsvaChange', {'hsva': values})
        );
    }

    getFormatValueUi(formatItems, changeCallback) {
        const container = this.inputs;

        formatItems.forEach(
            ([name, max]) => container.appendChild(this.getValueUi(name, max, changeCallback)));

        this.sliders = container.querySelectorAll('input[type="range"]');

        this.numbers = container.querySelectorAll('input[type="number"]');
    }

    removeFormatValueUi() {
        const container = this.inputs;

        while (container.lastChild) {
            container.removeChild(container.lastChild);
        }
    }

    getValueUi(name, max, changeCallback) {
        const container = document.createElement('div');

        const label = document.createElement('span');

        label.appendChild(document.createTextNode(name));

        container.appendChild(label);

        const slider = ColorPicker.getInputElement('range', 0, max),
            number = ColorPicker.getInputElement('number', 0, max);

        slider.addEventListener('input', () => changeCallback(this.getSliderValues()));

        container.appendChild(slider);

        number.addEventListener('input', () => changeCallback(this.getNumberValues()));

        container.appendChild(number);

        return container;
    }

    getSliderValues() {
        return Array.from(this.sliders).map((element) => element.value);
    }

    setSliderValues(color) {
        Array.from(this.sliders).forEach((element, index) => element.value = color[index]);
    }

    getNumberValues() {
        return Array.from(this.numbers).map((element) => element.value);
    }

    setNumberValues(color) {
        Array.from(this.numbers).forEach((element, index) => element.value = color[index]);
    }

    setPreviewValue(rgba) {
        this.preview.style.backgroundColor = rgba;
    }

    getHexValue() {
        return this.hex.value;
    }

    setHexValue(hexString) {
        this.hex.value = hexString;
    }
}
