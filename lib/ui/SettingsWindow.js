import BaseWindow from './BaseWindow.js';

export default class SettingsWindow extends BaseWindow {
    constructor(eventManager, container) {
        super(eventManager, container);

        const settingsClose = document.getElementById('settings-hide');

        settingsClose.addEventListener('click', () => this.manager.emit('closeSettingsWindow'));

        this.form = document.getElementById('settings-form');

        Array.from(this.form.elements['color-format']).forEach((element) =>
            element.addEventListener('click', () => {
                this.manager.emit(
                    'settingsChange',
                    {'key': 'color-format', 'value': element.value});

                this.manager.emit('pickerReplace', {'format': element.value});
            }));

        this.debug = document.getElementById('settings-debug');

        this.updateDebug();
    }

    updateDebug() {
        const container = this.debug;

        while (container.lastChild) {
            container.removeChild(container.lastChild);
        }

        const messages = {
            'size': `${window.innerHeight}x${window.innerWidth}`,
            'mobile': window.matchMedia('(pointer: coarse)').matches,
            'coarse pointer': window.matchMedia('(pointer: coarse)').matches,
            'any coarse pointer': window.matchMedia('(any-pointer: coarse)').matches,
            'fine pointer': window.matchMedia('(pointer: fine)').matches,
            'any fine pointer': window.matchMedia('(any-pointer: fine)').matches,
            'hover': window.matchMedia('(hover: hover)').matches,
            'any hover': window.matchMedia('(any-hover: hover)').matches,
        }

        Object.entries(messages).forEach(
            ([name, value]) => this.addDebugMessage(name, value));
    }

    addDebugMessage(name, value) {
        const messageContainer = document.createElement('div');

        const nameElement = document.createElement('span');

        nameElement.appendChild(document.createTextNode(`${name}: `));

        messageContainer.appendChild(nameElement);

        const valueElement = document.createElement('span');

        valueElement.appendChild(document.createTextNode(value));

        messageContainer.appendChild(valueElement);

        this.debug.appendChild(messageContainer);
    }

    updateSettings(valueMap) {
        this.form.elements['color-format'].value = valueMap.get('color-format');
    }
}