export default class Palette {
    constructor(eventManager) {
        this.manager = eventManager;

        const addButton = document.getElementById('palette-add');

        addButton.addEventListener('click', () => this.manager.emit('paletteAdd'));

        const clearButton = document.getElementById('palette-clear');

        clearButton.addEventListener('click', () => this.manager.emit('paletteClear'));

        const manageButton = document.getElementById('palette-manage');

        manageButton.addEventListener('click', () => this.manager.emit('openPaletteWindow'));

        this.paletteColors = document.getElementById('palette-colors');
    }

    static getRgbaString(r, g, b, a) {
        return `rgba(${r}, ${g}, ${b}, ${a / 255})`;
    }

    add(color) {
        const newPaletteColor = document.createElement('div');

        newPaletteColor.classList.add('palette-color');

        newPaletteColor.style.backgroundColor = Palette.getRgbaString(...color);

        newPaletteColor.addEventListener(
            'click', () => this.manager.emit('palettePick', {'color': color}));

        this.paletteColors.appendChild(newPaletteColor);
    }

    clear() {
        const container = this.paletteColors;

        while (container.lastChild) {
            container.removeChild(container.lastChild);
        }
    }
}