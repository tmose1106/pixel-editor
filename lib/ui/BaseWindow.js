export default class BaseWindow {
    constructor(eventManager, container) {
        this.manager = eventManager;

        this.window = container;
    }

    show() {
        this.window.classList.remove('hidden');
    }

    hide() {
        this.window.classList.add('hidden');
    }
}