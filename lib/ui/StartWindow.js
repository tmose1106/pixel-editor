import BaseWindow from './BaseWindow.js';

export default class StartWindow extends BaseWindow {
    constructor(eventManager, container) {
        super(eventManager, container);

        const canvasDimensionsForm = document.getElementById('canvas-dimensions');

        this.widthInput = canvasDimensionsForm.elements['width'];

        this.widthInput.value = 32;

        this.heightInput = canvasDimensionsForm.elements['height'];

        this.heightInput.value = 32;

        const canvasCreateButton = document.getElementById('canvas-create');

        canvasCreateButton.addEventListener('click', () => {
            this.manager.emit('editorConfigure', {
                'width': this.widthInput.value,
                'height': this.heightInput.value,
            });

            this.manager.emit('closeStartWindow');
        });
    }
}