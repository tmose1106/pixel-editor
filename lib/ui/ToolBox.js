export default class ToolBox {
    constructor(eventManager, toolMap) {
        this.manager = eventManager;

        const toolContainer = document.getElementById('tool-container');

        Object.entries(toolMap).forEach(([toolName, toolClass]) => {
            const button = document.createElement('button');

            button.addEventListener('click', () => {
                this.manager.emit('toolPick', {'tool': toolClass});
            });

            button.appendChild(document.createTextNode(toolName));

            toolContainer.appendChild(button);
        });
    }
}